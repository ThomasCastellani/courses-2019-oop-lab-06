package it.unibo.oop.lab.collections1;


import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    
    private static final int ELEMS = 100000;
    private static final int TO_MS = 1000000;
    
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
    	
    	List<Integer> list = new ArrayList<>();
    	
    	for (int i = 1000; i < 2000; i++) {
    		list.add(i);
    	}
    	
    	List<Integer> list2 = new LinkedList<>();
    	list2.addAll(list);
    	
    	int first = list.get(0);
    	int temp = first;
    	int last = list.get(list.size()-1);
    	list.set(0, last);
    	list.set(list.size() - 1, temp );
    	
    	/*
    	for (Integer num : list) {
    		System.out.println(num);
    	}
    	*/
    	
    	List<Integer> list3 = new ArrayList<>();
    	List<Integer> list4 = new LinkedList<>();
    	
    	//long time1 = System.nanoTime();
    	
    	for (int i = 0; i <= ELEMS; i++) {
    		list3.add(0, i);
    	}
    	
    	//time1 = System.nanoTime() - time1;
    			
    	//long time2 = System.nanoTime();
    			
    	for (int i = 0; i <= ELEMS; i++) {
    		list4.add(0, i);
    	}
    			
    	//time2 = System.nanoTime() - time2;
    	/*
    	System.out.println("ArrayList: " + time1 / TO_MS + " ms");
    	System.out.println("LinkedList: " + time2 / TO_MS + " ms");
    	*/
    	
    	long time1 = System.nanoTime();
    	
    	for(int i = 0; i <= 1000; i++) {
    		int elem = list3.get(list3.size()/2);
    	}
    	
    	time1 = System.nanoTime() - time1;
    	
    	long time2 = System.nanoTime();
    	
    	for(int i = 0; i <= 1000; i++) {
    		int elem = list4.get(list4.size()/2);
    	}
    	
    	time2 = System.nanoTime() - time2;
    	
    	System.out.println("ArrayList: " + time1 / TO_MS + " ms");
    	System.out.println("LinkedList: " + time2 / TO_MS + " ms");
    	
    	Map<String, Long> map = new HashMap<>();
    	
    	map.put("Africa", 1110635000L);
    	map.put("Americas", 972005000L);
    	map.put("Antarctica", 0L);
    	map.put("Asia", 4298723000L);	
    	map.put("Europe", 742452000L);
    	map.put("Oceania", 38304000L);
    	
    	
    	System.out.println(map);
    }
}
